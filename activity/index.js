console.log("Hello Mike")

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log('This Pokemon tackled targetPokemon');
		console.log("targetPokemon's health is now reduce to _targetPokemonhealth_")
	},
	faint: function() {
		console.log("Pokemon fainted")
	}
}

function Pokemon(name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = 2 * level;

	//methods
	this.tackle = function(target) {
		console.log(`${this.name} tackled ${target.name}`);
		console.log("Target Pokemon's health is now reduced.")
	};
	this.faint = function() {
		console.log(`${this.name} fainted.`)
	}
}


let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);
rattata.tackle(pikachu);


rattata.faint();


let trainer = {}

	trainer.name= "Ash Ketchum";
	trainer.age= 10;
	trainer.pokemon = ["Snorlax","Pikachu","Squirtle","Bulbasaur"]
	trainer.friends= {
		kanto: ["misty","brook",],
		hoenn: ["May","zMax"]
}

trainer.talk = function(){
console.log("Pikachu! I Choose You!");

}

console.log(trainer);

trainer.talk();


function Pokemon(name, level){
	this.name=name;
	this.level= level;
	this.health= 2*level;
	this.attack = 2*level;
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`)
		target.health-= this.attack;
		console.log(`${target.name}'s health is now reduced to ${target.health}`)
		if(target.health <= 0){
			target.faint()
		}
	}
	this.faint = function(){
		console.log(this.name + "fainted.")
	}
}


	let snorlax = new Pokemon("Snorlax", 30);
	console.log(snorlax);

	let squirtle = new Pokemon("squirtle", 25);
	console.log(squirtle)


	snorlax.tackle(squirtle);