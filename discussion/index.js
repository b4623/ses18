console.log("Hello World")


let cellPhone = {
	name : "Nokia 3310",
	manufactureDate: 1999
}

console.log(cellPhone);
console.log(typeof cellPhone);

console.log(cellPhone.name)

let cellPhone1 = {
	name: "Real Me Super Zoom",
	manufactureDate: 2019,
}

let cellPhone2 = {
	name: "Samsung A7",
	manufactureDate: 2017
}

function laptop(name, os , price){
	this.name = name;
	this.os = os;
	this.price = price;
}

let laptop1 = new laptop ("HP", "Windows 11", 32000);
console.log(laptop1)

let myLaptop = new laptop ( "MacBook Pro", "Catalina", 50000)
console.log(myLaptop)


let computer = {};
let myComputer = new Object();

console.log(laptop1.name);

console.log(laptop1["name"])

let array = [laptop1, myLaptop];
console.log(array);


let car = {};
console.log(car);
car.name = "Honda Civic";
console.log("Result from adding properties")
console.log(car);

car[ "manufacture date"] = 2019;
console.log(car);
console.log(car["manufacture date"]);

delete car['manufacture date'];
console.log("Result from deleting properties")
console.log(car);

car.name= "dodge charger R/T";
console.log("reassigning properties")
console.log(car)

let person = {
	name: "John",
	talk: function(){
		console.log(`Hello My Name is ${this.name}`)
	}
}
person.talk();
console.log(person);

person.walk = function(){
	console.log(this.name + "walk 25 steps forward")
}

person.walk();
 let friend = {
 	firstName: "Joe",
 	lastName: "Smith",
 	adress: {
 		city: "caloocan",
 		brgy: "bagong silang",
 	},
 	emails: [ 'joe@mail.com', 'joesmith@mail.com.xyz'],
 	introduce: function(){
 		console.log(` Hello My Name is ${this.firstName} ${this.lastName},
 					I Live at ${this.address.city}, ${this.address.brgy}`)
 		friend.introduce()
 	}
 }